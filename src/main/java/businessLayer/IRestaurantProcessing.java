package businessLayer;

import java.util.HashSet;
import java.util.Set;

import presentation.ChefGUI;

/**				This interface contains declared methods that should be implemented
 * @author BalaneanDan
 * @date May 7, 2020
 */
public interface IRestaurantProcessing {
	
	
		
	
		
		public void addMenuContent (MenuItem item) ;
		
	
		public void deleteItem (MenuItem item);
		
		
		public void editItem (MenuItem currItem, MenuItem newItem);
		
		
		public void createNewItem(HashSet<MenuItem> products,String itemName);
		// modif
		public void addOrder(Order o, Set<MenuItem> menuItems  );
		
		public float calculateOrderCost(Order or);
		boolean isWellFormed() ;

		public void observerAdd(ChefGUI cgui);
		
		public HashSet<MenuItem> getMenuContent() ;
		public void computeCheckOutBill(Order o, String file) ;
		
	

}
