package businessLayer;

import java.io.Serializable;

public abstract class MenuItem implements Serializable {
	
	private static int nr = 0;
	private int itemId;
	private String itemName;

	public MenuItem(String s) 
	{
		this.itemName = s;
		nr++;
		this.itemId = nr;
		
	}
	
	public abstract float computePrice();



	public int getItemId() {
		return itemId;
	}

	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	@Override
	public String toString() {
		return "Item Id:" + itemId + ", Name:" + itemName;
	}
	
	
}
