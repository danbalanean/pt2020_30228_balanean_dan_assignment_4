package businessLayer;

import java.io.Serializable;

/**				This  class extends the MenuItem, and represents the base product .
 * @author BalaneanDan
 *  May 7, 2020
 */
public class BaseProduct extends MenuItem implements Serializable{

	private float productPrice;
	
	public BaseProduct(String s, float p) {
		super(s);
		this.productPrice=p;
		
	}
	

	/**
	 *	Returns the computed price.
	 */
	@Override
	public float computePrice() {
		
		return this.productPrice;
	}

	public float getProductPrice() {
		return this.productPrice;
	}

	public void setProductPrice(float productPrice) {
		this.productPrice = productPrice;
	}
	
	

}
