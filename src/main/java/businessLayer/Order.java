package businessLayer;

import java.io.Serializable;
import java.time.LocalDate;

/**	
 * 		This class represents a real order in a restaurant.
 * @author BalaneanDan	
 * @date May 7, 2020
 */
public class Order implements Serializable {
	
	private int OrderID;
	private int table;
	private LocalDate Date;
	private static int nr;
	/**
	 * 	Simple constructor, increments the id by using a static vairable nr.
	 */
	public Order() 
	{
		nr++;
		this.OrderID = nr;
	}

	

	public int getOrderID()
	{
		return OrderID;
	}

	public void setOrderID(int orderID) 
	{
		OrderID = orderID;
	}

	public int getTable() 
	{
		return table;
	}

	public void setTable(int table) 
	{
		this.table = table;
	}

	public LocalDate getDate() 
	{
		return Date;
	}

	public void setDate(LocalDate date) 
	{
		Date = date;
	}

	@Override
	public int hashCode() 
	{
		// TODO Auto-generated method stub
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) 
	{
		// TODO Auto-generated method stub
		return this == obj;
	}

	@Override
	public String toString() 
	{
	
		return "id: " + OrderID + " table: " + table;
	}
	
	
}
