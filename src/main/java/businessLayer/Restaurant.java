package businessLayer;


import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

import dataLayer.FileWriter;
import presentation.ChefGUI;


/**
 * @author BalaneanDan
 * May 7, 2020
 */
public class Restaurant extends Observable implements IRestaurantProcessing, Serializable  {
	private HashMap<Order,HashSet<MenuItem>> requestedItems;
	private HashSet <MenuItem> menuContent;

	/**
	 * 		Basic constructor, used to initialize the  fields
	 */
	public Restaurant() {
		super();
		this.requestedItems = new HashMap<Order,HashSet<MenuItem>>();
		this.menuContent = new HashSet <MenuItem> ();
		
	}
	
	/**				
	 * @return Returns the producs available in the restaurant
	 */
	public HashSet<MenuItem> getMenuContent() {
		return menuContent;
	}
	/**
	 * @return Returns the ordered items that are stored in groups by orders in which are included.
	 */
	public HashMap<Order, HashSet<MenuItem>> getRequestedItems() {
		return requestedItems;
	}
	/**
	 * @param requestedItems Sets the ordered items that are stored in groups by orders in which are included.
	 * 			
	 */
	public void setRequestedItems(HashMap<Order, HashSet<MenuItem>> requestedItems) {
		this.requestedItems = requestedItems;
	}
	
	
	/**			Adds a new orderable item to the restaurant menu.
	 * @param item Receives the item to be added.
	 * @pre.custom.my  menuContent.contains(item) == true .Through this assert I make sure that the item id is greater than 0
	 * 
	 */
	public void addMenuContent (MenuItem item) 
	{
		assert item.getItemId() > 0;
		this.menuContent.add(item);
	}
	
	/**				This method removes an element from menuContent
	 * @param item	The item to be removed.
	 */
	public void deleteItem (MenuItem item)
	{
		assert this.menuContent.contains(item) == true;
		this.menuContent.remove(item);
		assert this.menuContent.contains(item) == false;
	}
	
	/**			This method is used to edit an existing item from the menuContent.
	 * 		It has to check before and after remove and add operations, if there are the specifiend params in the menuContent.
	 * @pre.custom.my (this.menuContent.contains(currItem) && !this.menuContent.contains(newItem)) == true
	 * @post.custom.my (!this.menuContent.contains(currItem) && this.menuContent.contains(newItem)) == true
	 * @param currItem
	 * @param newItem
	 */
	public void editItem (MenuItem currItem, MenuItem newItem)
	{
		assert (this.menuContent.contains(currItem) && !this.menuContent.contains(newItem)) == true;
		this.menuContent.remove(currItem);
		this.menuContent.add(newItem);
		assert (!this.menuContent.contains(currItem) && this.menuContent.contains(newItem)) == true;
	}
	
	
	/**				This method decides if it's a a simple product or not and add it to the menu.
	 * 
	 * @param products 	Receives a set of products (that could be components for a CompositeProduct)
	 * @param itemName	Receives a name for the new created item.
	 * @pre.custom.my  assert item.getItemId() > 0 == true  Verify the item id to be greater than 0.
	 * @post.custom.my assert this.menuContent.contains(item) == true Verify if the menuContent contains the new created element after adding it. 
	 */
	public void createNewItem(HashSet<MenuItem> products,String itemName)
	{	
		
		if(products.size()>1) {
			CompositeProduct item = new CompositeProduct(itemName,products);
			assert item.getItemId()> 0 == true;
			this.menuContent.add(item);
			assert this.menuContent.contains(item) == true;
		}
		else
		{
			MenuItem item = (MenuItem) products.iterator().next();
			assert item.getItemId() > 0 == true;
			this.menuContent.add(item);
			assert this.menuContent.contains(item) == true;
		}
	}
	
	/**
	 *		This method adds an order th the rerstaurnat data. 
	 */
	public void addOrder(Order o, Set<MenuItem> menuItems  )
	{
		assert o.getOrderID() > 0 == true;
		this.requestedItems.put(o, (HashSet<MenuItem>) menuItems );
		setChanged();
		notifyObservers();
	}
	
	/**
	 *		Receives an order and calculate the price for it.
	 *		Assert to verify the order id 
	 */
	public float calculateOrderCost(Order or)
	{	
		float sum=0;
		assert or.getOrderID() > 0 == true;
		for(MenuItem m:this.requestedItems.get(or))
		{
			sum+=m.computePrice();
		}
		
		return sum;
	}
	
	 /**
	 *		verify if the order is well formed 
	 *		return 0 in that case
	 */
	public boolean isWellFormed() {
		int size = 0;
		Set<MenuItem> menuItems = new HashSet<MenuItem>();
		for (Order o : this.requestedItems.keySet())
		{
			for (MenuItem item : this.requestedItems.get(o)) 
			{
				menuItems.add(item);
				size++;
			}
		}
		return (size == menuItems.size() );
	}

	public void observerAdd(ChefGUI o) {
		this.addObserver( (Observer) o);	
	}

	@Override
	public void computeCheckOutBill(Order o, String file) {
		// TODO Auto-generated method stub
		FileWriter.checkOutBill(o, this);
	}

	

	/**
	 *	compute the price of the order
	 */
	
	
}
