package businessLayer;

import java.util.HashSet;
import java.util.Set;

/**					This class represents an object formed from some product items 
 * @author BalaneanDan
 * @date May 7, 2020
 */
public class CompositeProduct extends MenuItem {
	private HashSet<MenuItem> containedItems;
	
	public CompositeProduct(String s, Set<MenuItem> items) {
		super(s);
		this.containedItems = new HashSet<MenuItem>();
		this.containedItems = (HashSet<MenuItem>) items;
	}
	@Override
	public float computePrice() 
	{
		
		int temp = 0;
		for(MenuItem c: containedItems)
			temp+=c.computePrice();

		return temp;
	}
	
	public void addComponent(MenuItem item) 
	{
		containedItems.add(item);
	}
	public void removeComponent(MenuItem item) 
	{
		containedItems.remove(item);
	}
	public HashSet<MenuItem> getComponents() 
	{
		return containedItems;
	}
	public void setComponents(HashSet<MenuItem> items) 
	{
		this.containedItems = items;
	}
	
}