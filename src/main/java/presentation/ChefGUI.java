package presentation;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import businessLayer.MenuItem;
import businessLayer.Order;

/**				Panel for Chef commands 
 * @author BalaneanDan
 *  May 7, 2020
 */
public class ChefGUI extends JPanel implements Observer {

	JButton home;
	private JTextField title;
	private JTextField todoList;
	private JTable table;
	DefaultTableModel model;

	public ChefGUI() {

		init();
	}

	private void init() {
		this.setLayout(null);
		this.setSize(800, 1600);
		this.setBounds(10, 10, 1000, 800);
		home = new JButton("Home");
		home.setBackground(new Color(157, 220, 235));
		home.setBounds(10, 20, 100, 50);
		home.setFont(new Font("Comic Sans MS", Font.BOLD, 14));
		this.add(home);

		// title
		title = new JTextField("Chef control panel");
		title.setBounds(300, 10, 320, 30);
		title.setBorder(null);
		title.setFont(new Font("Comic Sans MS", Font.BOLD, 24));
		title.setEditable(false);
		this.add(title);
		// todo list
		title = new JTextField("Products List: ");
		title.setBounds(200, 150, 320, 30);
		title.setBorder(null);
		title.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
		title.setEditable(false);
		this.add(title);

		// table
		table = new JTable();
		table.setFont(new Font("Source Sans Pro Semibold", Font.PLAIN, 13));
		table.setBackground(new Color(255, 228, 225));

		table.setBounds(837, 650, 500, 50);
		
		 model = (DefaultTableModel) table.getModel();

		while (model.getRowCount() > 0) {
			model.removeRow(0);
		}
		Object[] columnsName = new Object[2];
		columnsName[0] = "comanda";
		columnsName[1] = "produse";

		model.setColumnIdentifiers(columnsName);

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(200, 207, 380, 400);
		this.add(scrollPane);
	}

	public void HomeBtnActionListener(ActionListener a) {
		this.home.addActionListener(a);
	}

	public void updateTable(String comanda,String produse) {
		

		model.addRow(new Object[] { comanda,produse });

		table.setModel(model);
	}

	@Override
	public void update(Observable o, Object arg) {
		// TODO Auto-generated method stub
		System.out.println("vazut de catre bucatar");
	}

	
}
