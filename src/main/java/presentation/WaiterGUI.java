package presentation;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import businessLayer.BaseProduct;
import businessLayer.MenuItem;
import businessLayer.Order;

public class WaiterGUI extends JPanel {

	JButton home;
	private JTextField title;
	private JTextField order;
	private JTextField orderNr;
	private JTextField activeOrders;
	private JTextField result;
	private JComboBox<MenuItem> itemsComboBox;
	private JButton createBtn;
	private JButton addBtn;
	private JButton removeBtn;
	private JButton computePriceBtn;
	private JLabel orderNrLbl;
	private JLabel itemLbl;

	//
	private JList jList;
	private JTextArea terminal;
	private JList jListForCreate;
	private JButton checkOutBtn;

	DefaultListModel<MenuItem> listModel;
	DefaultListModel<Order> activeOrdersListModel;

	private static final String[] listItems = { "BLUE", "BLACK", "CYAN", "GREEN", "GRAY", "RED", "WHITE" };
	private static final Color[] colors = { Color.BLUE, Color.BLACK, Color.CYAN, Color.GREEN, Color.GRAY, Color.RED,
			Color.WHITE };

	public WaiterGUI() {

		init();
	}

	private void init() {
		// this.setBackground(new Color(113, 76, 0));
		this.setLayout(null);
		this.setSize(800, 1600);
		this.setBounds(10, 10, 1000, 800);
		home = new JButton("Home");
		home.setBackground(new Color(157, 220, 235));
		home.setBounds(10, 20, 100, 50);
		home.setFont(new Font("Comic Sans MS", Font.BOLD, 14));
		this.add(home);
		// title
		title = new JTextField("Waiter control panel");
		title.setBounds(300, 10, 320, 30);
		title.setBorder(null);
		title.setFont(new Font("Comic Sans MS", Font.BOLD, 24));
		title.setEditable(false);
		this.add(title);

		order = new JTextField("Create a new order");
		order.setBounds(100, 200, 200, 20);
		order.setBorder(null);
		order.setFont(new Font("Comic Sans MS", Font.BOLD, 20));
		order.setEditable(false);
		this.add(order);

		// items
		itemsComboBox = new JComboBox<MenuItem>();

		// for (MenuItem m : ((Restaurant) restaurant).getMenu()) {
		// itemsComboBox.addItem(m);

		// }
		// create btn
		createBtn = new JButton("Create order");
		createBtn.setBounds(100, 550, 200, 30);
		createBtn.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		this.add(createBtn);
		itemsComboBox.setBounds(100, 480, 200, 22);
		this.add(itemsComboBox);
		itemsComboBox.setBounds(100, 480, 200, 22);
		this.add(itemsComboBox);

		// lbl si button
		itemLbl = new JLabel("Select an item:");
		itemLbl.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		itemLbl.setBounds(100, 460, 197, 15);
		this.add(itemLbl);

		addBtn = new JButton("Add");
		addBtn.setBounds(100, 510, 80, 25);
		addBtn.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		addBtn.setVisible(true);
		this.add(addBtn);

		removeBtn = new JButton("Remove");
		removeBtn.setBounds(200, 510, 100, 25);
		removeBtn.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		removeBtn.setVisible(true);
		this.add(removeBtn);

		// create list
		listModel = new DefaultListModel<>();
		
		jListForCreate = new JList(listModel);
		jListForCreate.setBounds(100, 290, 200, 150);
		this.add(jListForCreate);

		jListForCreate.setFixedCellHeight(15);
		jListForCreate.setFixedCellWidth(100);
		jListForCreate.setVisibleRowCount(4);
		jListForCreate.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		this.add(jListForCreate);

		// order nr

		orderNr = new JTextField();
		orderNr.setBounds(220, 250, 100, 30);
		orderNr.setFont(new Font("Comic Sans MS", Font.BOLD, 24));
		this.add(orderNr);

		// lbl order nr
		orderNrLbl = new JLabel("Table:");
		orderNrLbl.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		orderNrLbl.setBounds(100, 250, 197, 15);
		this.add(orderNrLbl);
		setLayout(null);

		// panels for orders
		activeOrdersListModel=new DefaultListModel<>();
		jList = new JList(activeOrdersListModel);
		this.add(jList);
		jList.setVisible(true);
		// jList.setBounds(400, 300, 100, 100);
		jList.setFixedCellHeight(15);
		jList.setFixedCellWidth(100);
		JScrollPane scrollPane = new JScrollPane();
		JList list = new JList();
		scrollPane.setViewportView(jList);
		scrollPane.setVisible(true);
		scrollPane.setBounds(400, 300, 200, 300);
		this.add(scrollPane);

		jList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

		this.terminal = new JTextArea();
		terminal.setBounds(700, 300, 200, 300);
		terminal.setEditable(false);
		this.add(terminal);

		

		// butoane check out si calculate price
		checkOutBtn = new JButton("Generate Bill >>");
		checkOutBtn.setBounds(400, 200, 200, 40);
		checkOutBtn.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		/*
		 * checkOutBtn.addActionListener(new ActionListener() {
		 * 
		 * @Override public void actionPerformed(ActionEvent e) {
		 * jListForCopy.setListData(jList.getSelectedValues()); }
		 * 
		 * });
		 */

		computePriceBtn = new JButton("Compute >>");
		computePriceBtn.setBounds(700, 200, 150, 40);
		computePriceBtn.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		this.add(computePriceBtn);
		this.add(checkOutBtn);

		activeOrders = new JTextField("Active Orders:");
		activeOrders.setBounds(420, 260, 220, 20);
		activeOrders.setBorder(null);
		activeOrders.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
		activeOrders.setEditable(false);
		this.add(activeOrders);

		result = new JTextField(" Computed:");
		result.setBounds(700, 260, 320, 20);
		result.setBorder(null);
		result.setFont(new Font("Comic Sans MS", Font.BOLD, 18));
		result.setEditable(false);
		this.add(result);

	}

	public void HomeBtnActionListener(ActionListener a) {
		this.home.addActionListener(a);
	}

	public void computeBtnActionListener(ActionListener a) {
		this.computePriceBtn.addActionListener(a);
	}

	public void checkoutBtnACtionListener(ActionListener a) {
		this.checkOutBtn.addActionListener(a);
	}

	public void addBtnACtionListener(ActionListener a) {
		this.addBtn.addActionListener(a);
	}

	public void removeBtnACtionListener(ActionListener a) {
		this.removeBtn.addActionListener(a);
	}

	public void createOrderBtnACtionListener(ActionListener a) {
		this.createBtn.addActionListener(a);
	}

	public JTextField getOrderNr() {
		return orderNr;
	}

	public void setOrderNr(JTextField orderNr) {
		this.orderNr = orderNr;
	}

	public JComboBox<MenuItem> getItemsComboBox() {
		return itemsComboBox;
	}

	public void setItemsComboBox(JComboBox<MenuItem> itemsComboBox) {
		this.itemsComboBox = itemsComboBox;
	}

	public JTextField getActiveOrders() {
		return activeOrders;
	}

	public void setActiveOrders(JTextField activeOrders) {
		this.activeOrders = activeOrders;
	}

	public JList getjList() {
		
		return jList;
	}

	public void setjList(JList jList) {
		this.jList = jList;
	}

	

	public JTextArea getTerminal() {
		return terminal;
	}

	public void setTerminal(JTextArea terminal) {
		this.terminal = terminal;
	}

	public DefaultListModel<Order> getActiveOrdersListModel() {
		return activeOrdersListModel;
	}

	

	public JList getjListForCreate() {
		return jListForCreate;
	}

	public void setjListForCreate(JList jListForCreate) {
		this.jListForCreate = jListForCreate;
	}

	public DefaultListModel<MenuItem> getListModel() {
		return listModel;
	}

	public void setListModel(DefaultListModel<MenuItem> listModel) {
		this.listModel = listModel;
	}

}
