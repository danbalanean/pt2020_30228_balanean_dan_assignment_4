package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.Set;

import businessLayer.BaseProduct;
import businessLayer.CompositeProduct;
import businessLayer.IRestaurantProcessing;
import businessLayer.MenuItem;
import businessLayer.Order;
import businessLayer.Restaurant;
import dataLayer.RestaurantSerializator;

/**
 * @author BalaneanDan
 *  May 7, 2020
 */
public class GUIManager {

	private DashboardGUI dashboard;
	private AdminGUI admin;
	private ChefGUI chef;
	private WaiterGUI waiter;

	public GUIManager(IRestaurantProcessing restaurant, String file) {
		super();
		this.dashboard = new DashboardGUI(restaurant);
		this.admin = new AdminGUI(restaurant);
		this.waiter = new WaiterGUI();
		this.chef = new ChefGUI();
		this.dashboard.setVisible(true);
		this.buttonsHomeListeners();
		this.adminListeners();
		this.waiterListeners(file);
	}

	private void buttonsHomeListeners() {
		this.dashboard.getAdminPanel().HomeBtnActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dashboard.hideAdmin();
				dashboard.setHome();
			}
		});

		this.dashboard.getWaiterPanel().HomeBtnActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dashboard.hideWaiter();
				dashboard.setHome();
			}
		});

		this.dashboard.getChefPanel().HomeBtnActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dashboard.hideChef();
				dashboard.setHome();
			}
		});

		this.dashboard.AdminBtnActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dashboard.setAdmin();
				dashboard.hideHome();
			}
		});

		this.dashboard.ChefBtnActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dashboard.setChef();
				dashboard.hideHome();
			}
		});

		this.dashboard.WaiterBtnActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dashboard.setWaiter();
				dashboard.hideHome();
				
			}
		});

	}

	private void adminListeners() {
		
		Restaurant rr = (Restaurant) dashboard.getAdminPanel().getRestaurant();
				rr.observerAdd(dashboard.getChefPanel());
		this.dashboard.getAdminPanel().RadioBtn1ActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dashboard.getAdminPanel().getItemPrice().setVisible(true);
				dashboard.getAdminPanel().getPriceLBL().setVisible(true);
				dashboard.getAdminPanel().getCreateComboBox().setVisible(false);
				dashboard.getAdminPanel().getComponentsLBL().setVisible(false);
				dashboard.getAdminPanel().getAddBtn().setVisible(false);
				dashboard.getAdminPanel().getTextArea().setVisible(false);
				dashboard.getAdminPanel().getRad2().setSelected(false);
				dashboard.getAdminPanel().getRad1().setSelected(true);
				dashboard.getAdminPanel().getCreateBtn().setVisible(true);
				dashboard.getAdminPanel().getCreateCompBtn().setVisible(false);
			}
		});
		this.dashboard.getAdminPanel().RadioBtn2ActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dashboard.getAdminPanel().getItemPrice().setVisible(false);
				dashboard.getAdminPanel().getPriceLBL().setVisible(false);
				dashboard.getAdminPanel().getCreateComboBox().setVisible(true);
				dashboard.getAdminPanel().getComponentsLBL().setVisible(true);
				dashboard.getAdminPanel().getAddBtn().setVisible(true);
				dashboard.getAdminPanel().getTextArea().setVisible(true);
				dashboard.getAdminPanel().getRad2().setSelected(true);
				dashboard.getAdminPanel().getRad1().setSelected(false);
				dashboard.getAdminPanel().getCreateBtn().setVisible(false);
				dashboard.getAdminPanel().getCreateCompBtn().setVisible(true);
			}
		});

		// panel buttons
		this.dashboard.getAdminPanel().createItem1BtnActionListener(new ActionListener() {
				
			@Override
			public void actionPerformed(ActionEvent e) {

				try {
					String name = dashboard.getAdminPanel().itemNameString();
					String price = dashboard.getAdminPanel().itemPriceString();
					if (name.equals("")) {
						throw new Exception("please enter a name!");
					}
					if (price.equals("")) {
						throw new Exception("Please enter a price!");
					}
					HashSet<MenuItem> element = new HashSet<MenuItem>();
					MenuItem item = new BaseProduct(name, Integer.parseInt(price));
					element.add(item);
					Restaurant restaurant = (Restaurant) dashboard.getAdminPanel().getRestaurant();
					restaurant.createNewItem(element, name);
					RestaurantSerializator.serializeRestaurant(restaurant);
					refreshTable(restaurant);
					System.out.println(restaurant.getMenuContent());
					refreshWaiterCmd(restaurant);

				} catch (Exception e1) {
					dashboard.getAdminPanel().showMessage(e1.toString());
				}
			}
		});
		final HashSet<MenuItem> components = new HashSet<MenuItem>();
		this.dashboard.getAdminPanel().createItem2BtnActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				System.out.println(dashboard.getAdminPanel().itemNameString());

				try {
					String name = dashboard.getAdminPanel().itemNameString();

					if (name.equals("")) {
						throw new Exception("please enter a name!");
					}
					if (components.size() == 0) {
						throw new Exception("No components!");
					}

					Restaurant restaurant = (Restaurant) dashboard.getAdminPanel().getRestaurant();

					HashSet<MenuItem> element = new HashSet<MenuItem>();
					MenuItem item = new CompositeProduct(name, components);
					for (MenuItem m : components) {
						item.computePrice();
					}
					element.add(item);

					restaurant.createNewItem(element, name);

					dashboard.getAdminPanel().getTextArea().setText("");
					RestaurantSerializator.serializeRestaurant(restaurant);
					refreshTable(restaurant);
					System.out.println(dashboard.getAdminPanel().getRestaurant().getMenuContent());
					components.clear();
					refreshWaiterCmd(restaurant);
				} catch (Exception e1) {
					dashboard.getAdminPanel().showMessage(e1.toString());
				}
			}
		});

	
		this.dashboard.getAdminPanel().addComponentActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

				try {

					components.add((MenuItem) dashboard.getAdminPanel().getCreateComboBox().getSelectedItem());

					dashboard.getAdminPanel().getTextArea().append(dashboard.getAdminPanel().getCreateComboBox().getSelectedItem().toString() + "\n");

				} catch (Exception e1) {
					dashboard.getAdminPanel().showMessage(e1.toString());
				}
			}
		});
		
		this.dashboard.getAdminPanel().editBtnActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub

				try {

						System.out.println(dashboard.getAdminPanel().getRestaurant().getMenuContent());
				} catch (Exception e1) {
					dashboard.getAdminPanel().showMessage(e1.toString());
				}
			}
		});
		
		this.dashboard.getAdminPanel().editBtnActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					
					MenuItem m =(MenuItem) dashboard.getAdminPanel().getEditComboBox().getSelectedItem();
					
					String name =dashboard.getAdminPanel().getEditItemName();
					int price = Integer.parseInt(dashboard.getAdminPanel().getEditItemPrice());
					MenuItem edited = new BaseProduct(name, price);
					edited.setItemId(m.getItemId());
					Restaurant restaurant = (Restaurant) dashboard.getAdminPanel().getRestaurant();
					restaurant.editItem(m, edited);
					RestaurantSerializator.serializeRestaurant(restaurant);
					refreshTable(restaurant);
				} catch (Exception e1) {

				}
			}
		});
		
		this.dashboard.getAdminPanel().deleteBtnActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					
					MenuItem m =(MenuItem) dashboard.getAdminPanel().getEditComboBox().getSelectedItem();
					
					Restaurant restaurant = (Restaurant) dashboard.getAdminPanel().getRestaurant();
					restaurant.deleteItem(m);
					RestaurantSerializator.serializeRestaurant(restaurant);
					refreshTable(restaurant);
				} catch (Exception e1) {

				}
			}
		});

	}
	
	public void refreshWaiterCmd(Restaurant restaurant)
	{
		///--------------------------------------------
		dashboard.getWaiterPanel().getItemsComboBox().removeAllItems();
		for (MenuItem m : ((Restaurant) restaurant).getMenuContent()) {
			dashboard.getWaiterPanel().getItemsComboBox().addItem(m);
			
		}
		
	}
	public void waiterListeners(final String file)
	{	
		final Restaurant restaurant = (Restaurant) dashboard.getAdminPanel().getRestaurant();
		
		
			
			System.out.println(restaurant.getMenuContent());
			this.dashboard.getWaiterPanel().getItemsComboBox().removeAllItems();
			for (MenuItem m : ((Restaurant) restaurant).getMenuContent()) {
				this.dashboard.getWaiterPanel().getItemsComboBox().addItem(m);
				
			}
			
		this.dashboard.getWaiterPanel().computeBtnActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try {
					if ((Order) dashboard.getWaiterPanel().getjList().getSelectedValue()== null) {
						throw new Exception("Select an order!");
					}
					
					Restaurant restaurant = (Restaurant) dashboard.getAdminPanel().getRestaurant();
					float price = restaurant.calculateOrderCost((Order) dashboard.getWaiterPanel().getjList().getSelectedValue());
					System.out.println(price);
					dashboard.getWaiterPanel().getTerminal().append("The price for selected order is :"+price+"\n");
				}catch (Exception ex)
				{
					dashboard.getAdminPanel().showMessage(ex.toString());
				}
				
			}
			
		});
		
		this.dashboard.getWaiterPanel().checkoutBtnACtionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				Restaurant restaurant = (Restaurant) dashboard.getAdminPanel().getRestaurant();
				restaurant.computeCheckOutBill((Order) dashboard.getWaiterPanel().getjList().getSelectedValue(), file);
				
				dashboard.getWaiterPanel().getTerminal().append("Generated a bill for  :"+(Order) dashboard.getWaiterPanel().getjList().getSelectedValue()+"\n");
				
			}
			
		});
		final HashSet<MenuItem> l=new HashSet<MenuItem>();
		this.dashboard.getWaiterPanel().addBtnACtionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				dashboard.getWaiterPanel().getListModel().addElement((MenuItem) dashboard.getWaiterPanel().getItemsComboBox().getSelectedItem());
				l.add((MenuItem) dashboard.getWaiterPanel().getItemsComboBox().getSelectedItem());
			}
			
		});
		this.dashboard.getWaiterPanel().removeBtnACtionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				dashboard.getWaiterPanel().getListModel().removeElement(dashboard.getWaiterPanel().getItemsComboBox().getSelectedItem());
				Restaurant restaurant = (Restaurant) dashboard.getAdminPanel().getRestaurant();
				refreshWaiterCmd(restaurant);
			}
			
		});
		
		this.dashboard.getWaiterPanel().createOrderBtnACtionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				try
				{
					if (dashboard.getWaiterPanel().getOrderNr().getText().equalsIgnoreCase("")) {
						throw new Exception("Enter a table nr!");
					}
					Order order = new Order();
					order.setTable(Integer.parseInt(dashboard.getWaiterPanel().getOrderNr().getText()));
					dashboard.getChefPanel().updateTable(order.toString(),dashboard.getWaiterPanel().getListModel().toString());
					dashboard.getWaiterPanel().getActiveOrdersListModel().addElement(order);
					restaurant.addOrder(order, l);
					
					System.out.println(restaurant.getRequestedItems().get(order));
					RestaurantSerializator.serializeRestaurant(restaurant);
				}catch(Exception ex)
				{
					dashboard.getAdminPanel().showMessage(ex.toString());
				}
				
				
			}
			
		});
	}
	public void refreshTable(Restaurant restaurant) {

		RestaurantSerializator.serializeRestaurant(restaurant);
		dashboard.getAdminPanel().updateSys();
		dashboard.getAdminPanel().revalidate();
		dashboard.getAdminPanel().repaint();
		dashboard.getAdminPanel().tableContent();

	}

}
