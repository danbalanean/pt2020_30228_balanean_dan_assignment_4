package presentation;

import java.awt.Color;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.LayoutManager;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import businessLayer.IRestaurantProcessing;

/**				Main panel of the application
 * @author BalaneanDan
 *  May 7, 2020
 */
public class DashboardGUI extends JFrame {
	JButton SelectAdminBtn;
	JButton SelectChefBtn;
	JButton SelectWaiterBtn;
	AdminGUI adminPanel;
	ChefGUI chefPanel;
	WaiterGUI waiterPanel;
	public DashboardGUI(IRestaurantProcessing restaurant) throws HeadlessException {
		super();

		init(restaurant);
	//	hideHome();

	}

	private void init(IRestaurantProcessing restaurant) {

		getContentPane().setBackground(new Color(66, 78, 150));
		setFont(new Font("Bitstream Charter", Font.PLAIN, 14));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Restaurant Dashboard");

		getContentPane().setLayout(null);
		this.setSize(800, 1600);
		this.setBounds(10, 10, 1000, 800);

		SelectAdminBtn = new JButton("Admin ");
		getContentPane().add(SelectAdminBtn);
		SelectAdminBtn.setBackground(new Color(155, 220, 235));
		SelectAdminBtn.setBounds(20, 50, 267, 84);
		SelectAdminBtn.setFont(new Font("Comic Sans MS", Font.BOLD, 27));

		SelectWaiterBtn = new JButton("Waiter");
		SelectWaiterBtn.setBackground(new Color(244, 200, 200));
		SelectWaiterBtn.setBounds(20, 150, 267, 84);
		SelectWaiterBtn.setFont(new Font("Comic Sans MS", Font.BOLD, 27));
		getContentPane().add(SelectWaiterBtn);

		SelectChefBtn = new JButton("Chef");
		this.SelectChefBtn.setBackground(new Color(210, 191, 216));
		this.SelectChefBtn.setBounds(20, 272, 267, 84);
		this.SelectChefBtn.setFont(new Font("Comic Sans MS", Font.BOLD, 27));
		getContentPane().add(SelectChefBtn);

		adminPanel = new AdminGUI(restaurant);
		getContentPane().add(adminPanel);
		adminPanel.setVisible(false);
		
		chefPanel = new ChefGUI();
		getContentPane().add(chefPanel);
		chefPanel.setVisible(false);
		
		waiterPanel = new WaiterGUI();
		getContentPane().add(waiterPanel);
		waiterPanel.setVisible(false);
	}
	
	protected void setAdmin()
	{
		adminPanel.setVisible(true);
	}
	protected void hideAdmin()
	{
		adminPanel.setVisible(false);
	}
	
	protected void setChef()
	{
		chefPanel.setVisible(true);
	}
	protected void hideChef()
	{
		chefPanel.setVisible(false);
	}
	
	protected void setWaiter()
	{
		waiterPanel.setVisible(true);
	}
	protected void hideWaiter()
	{
		waiterPanel.setVisible(false);
	}
	
	
	protected void hideHome()
	{
		 SelectAdminBtn.setVisible(false);
		 SelectChefBtn.setVisible(false);
		 SelectWaiterBtn.setVisible(false);
	}
	protected void setHome()
	{
		 SelectAdminBtn.setVisible(true);
		 SelectChefBtn.setVisible(true);
		 SelectWaiterBtn.setVisible(true);
	}
	public void AdminBtnActionListener(ActionListener a) {
		this.SelectAdminBtn.addActionListener(a);
	}

	public void WaiterBtnActionListener(ActionListener a) {
		this.SelectWaiterBtn.addActionListener(a);
	}

	public void ChefBtnActionListener(ActionListener a) {
		this.SelectChefBtn.addActionListener(a);
	}

	public AdminGUI getAdminPanel() {
		return adminPanel;
	}

	public void setAdminPanel(AdminGUI adminPanel) {
		this.adminPanel = adminPanel;
	}

	public ChefGUI getChefPanel() {
		return chefPanel;
	}

	public void setChefPanel(ChefGUI chefPanel) {
		this.chefPanel = chefPanel;
	}

	public WaiterGUI getWaiterPanel() {
		return waiterPanel;
	}

	public void setWaiterPanel(WaiterGUI waiterPanel) {
		this.waiterPanel = waiterPanel;
	}
	

}
