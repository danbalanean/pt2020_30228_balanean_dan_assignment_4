package presentation;

import java.awt.Color;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import businessLayer.IRestaurantProcessing;
import businessLayer.MenuItem;
import businessLayer.Restaurant;

/**				Panel for admin commands
 * @author BalaneanDan
 *  May 7, 2020
 */
public class AdminGUI extends JPanel {

	// ----------------------------------------
	private JComboBox<MenuItem> createComboBox;
	private JComboBox<MenuItem> editComboBox;
	private JComboBox<MenuItem> deleteComboBox;
	JButton home;
	private JTextField title;
	private JTextField createItem;
	private JTextField editItem;
	private JTextField deleteItem;
	private JTextField menuContent;
	private JTextField itemName;
	private JTextField itemPrice;
	private JTextField eItemName;
	private JTextField eItemPrice;
	JRadioButton rad1;
	JRadioButton rad2;
	JLabel priceLBL;
	JLabel nameLBL;
	JLabel ePriceLBL;
	JLabel eNameLBL;
	JLabel componentsLBL;
	private TextArea textArea;
	JButton addBtn;
	JButton createBtn;
	JButton createCompBtn;
	JButton deleteBtn;
	JButton editBtn;
	JTable table;
	private IRestaurantProcessing restaurant;

	public AdminGUI(IRestaurantProcessing restaurant) {
		this.restaurant = restaurant;

		init();

	}

	private void init() {
		this.setLayout(null);
		this.setSize(800, 1600);
		this.setBounds(10, 10, 1000, 800);
		// home button
		home = new JButton("Home");
		home.setBackground(new Color(157, 220, 235));
		home.setBounds(5, 5, 100, 50);
		home.setFont(new Font("Comic Sans MS", Font.BOLD, 14));
		this.add(home);
		// title
		title = new JTextField("Administrator control panel");
		title.setBounds(300, 10, 320, 30);
		title.setBorder(null);
		title.setFont(new Font("Comic Sans MS", Font.BOLD, 24));
		title.setEditable(false);
		this.add(title);
		// create item
		createItem = new JTextField(" Create Items");
		createItem.setBounds(100, 100, 200, 30);
		createItem.setBorder(null);
		createItem.setFont(new Font("Comic Sans MS", Font.BOLD, 24));
		createItem.setEditable(false);
		this.add(createItem);
		// menu content

		menuContent = new JTextField("Menu content");
		menuContent.setBounds(700, 100, 320, 30);
		menuContent.setBorder(null);
		menuContent.setFont(new Font("Comic Sans MS", Font.BOLD, 24));
		menuContent.setEditable(false);
		this.add(menuContent);

		// radio btns
		rad1 = new JRadioButton("simple");
		rad1.setMnemonic(1);
		rad1.setActionCommand("cmd");
		rad1.setSelected(true);
		rad1.setVisible(true);
		rad1.setBounds(100, 150, 100, 50);
		this.add(rad1);

		rad2 = new JRadioButton("composite");
		rad2.setMnemonic(1);
		rad2.setActionCommand("cmd");
		rad2.setSelected(false);
		rad2.setVisible(true);
		rad2.setBounds(200, 150, 100, 50);
		this.add(rad2);
		//

		//
		// simple item
		nameLBL = new JLabel("Name:");
		nameLBL.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		nameLBL.setBounds(100, 200, 197, 15);
		this.add(nameLBL);

		priceLBL = new JLabel("Price:");
		priceLBL.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		priceLBL.setBounds(100, 230, 197, 15);
		this.add(priceLBL);

		itemName = new JTextField();
		itemName.setBounds(150, 200, 150, 22);
		this.add(itemName);

		itemPrice = new JTextField();
		itemPrice.setBounds(150, 230, 150, 22);
		this.add(itemPrice);

		// btn
		createBtn = new JButton("Create simple item");
		createBtn.setBounds(100, 300, 200, 30);
		createBtn.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		this.add(createBtn);

		createCompBtn = new JButton("Create item");
		createCompBtn.setBounds(100, 300, 200, 30);
		createCompBtn.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		this.add(createCompBtn);
		createCompBtn.setVisible(false);

		// composite item
		createComboBox = new JComboBox<MenuItem>();



		createComboBox.setBounds(150, 230, 150, 22);
		this.add(createComboBox);

		componentsLBL = new JLabel("Comp:");
		componentsLBL.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		componentsLBL.setBounds(100, 230, 197, 15);
		this.add(componentsLBL);
		componentsLBL.setVisible(false);

		textArea = new TextArea();
		textArea.setBounds(320, 165, 250, 150);
		textArea.setVisible(false);
		this.add(textArea);

		addBtn = new JButton("Add component");
		addBtn.setBounds(150, 270, 150, 22);
		addBtn.setVisible(false);
		this.add(addBtn);

		/// menu content
		table = new JTable();
		table.setFont(new Font("Source Sans Pro Semibold", Font.PLAIN, 13));
		table.setBackground(new Color(255, 228, 225));

		table.setBounds(637, 353, 222, -262);

		//this.tableContent();

		JScrollPane scrollPane = new JScrollPane(table);
		scrollPane.setBounds(580, 167, 380, 400);
		this.add(scrollPane);

		// edit

		editItem = new JTextField(" Edit Items");
		editItem.setBounds(120, 350, 200, 30);
		editItem.setBorder(null);
		editItem.setFont(new Font("Comic Sans MS", Font.BOLD, 24));
		editItem.setEditable(false);
		this.add(editItem);

		eNameLBL = new JLabel("Name:");
		eNameLBL.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		eNameLBL.setBounds(100, 430, 197, 15);
		this.add(eNameLBL);

		ePriceLBL = new JLabel("Price:");
		ePriceLBL.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		ePriceLBL.setBounds(100, 460, 197, 15);
		this.add(ePriceLBL);

		eItemName = new JTextField();
		eItemName.setBounds(150, 430, 150, 22);
		this.add(eItemName);

		eItemPrice = new JTextField();
		eItemPrice.setBounds(150, 460, 150, 22);
		this.add(eItemPrice);

		editComboBox = new JComboBox<MenuItem>();

		editComboBox.setBounds(100, 400, 200, 22);
		this.add(editComboBox);

		editBtn = new JButton("Edit item");
		editBtn.setBounds(100, 490, 200, 30);
		editBtn.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		this.add(editBtn);

		// delete

		deleteItem = new JTextField(" Delete Items");
		deleteItem.setBounds(120, 540, 200, 30);
		deleteItem.setBorder(null);
		deleteItem.setFont(new Font("Comic Sans MS", Font.BOLD, 24));
		deleteItem.setEditable(false);
		this.add(deleteItem);

		deleteComboBox = new JComboBox<MenuItem>();

		deleteComboBox.setBounds(100, 590, 200, 22);
		this.add(deleteComboBox);

		deleteBtn = new JButton("Delete item");
		deleteBtn.setBounds(100, 630, 200, 30);
		deleteBtn.setFont(new Font("Comic Sans MS", Font.BOLD, 15));
		this.add(deleteBtn);
		
		createComboBox.removeAllItems();
		deleteComboBox.removeAllItems();
		editComboBox.removeAllItems();
		for (MenuItem m : ((Restaurant) restaurant).getMenuContent()) {
			createComboBox.addItem(m);
			deleteComboBox.addItem(m);
			editComboBox.addItem(m);
			
		}
		tableContent();
	}

	
	public void RadioBtn1ActionListener(ActionListener a) {
		this.rad1.addActionListener(a);
	}

	public void RadioBtn2ActionListener(ActionListener a) {
		this.rad2.addActionListener(a);
	}

	public void HomeBtnActionListener(ActionListener a) {
		this.home.addActionListener(a);

	}
	
	public void editBtnActionListener(ActionListener a) {
		this.editBtn.addActionListener(a);

	}
	public void deleteBtnActionListener(ActionListener a) {
		this.deleteBtn.addActionListener(a);

	}

	public void createItem1BtnActionListener(ActionListener a) {
		this.createBtn.addActionListener(a);

	}

	public void createItem2BtnActionListener(ActionListener a) {
		this.createCompBtn.addActionListener(a);

	}
	public void addComponentActionListener(ActionListener a) {
		addBtn.addActionListener(a);
	}
	
	public void showMessage(final String s) {
		JOptionPane.showMessageDialog(this, s);
	}

	public void updateSys() {
		createComboBox.removeAllItems();
		deleteComboBox.removeAllItems();
		editComboBox.removeAllItems();
		for (MenuItem m : ((Restaurant) restaurant).getMenuContent()) {
			createComboBox.addItem(m);
			deleteComboBox.addItem(m);
			editComboBox.addItem(m);
		}
	}

	public void tableContent() {
		
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		
		while(model.getRowCount() > 0)
		{
		    model.removeRow(0);
		}
		Object[] columnsName = new Object[3];
		columnsName[0] = "id";
		columnsName[1] = "name";
		columnsName[2] = "price";

		model.setColumnIdentifiers(columnsName);

		Object[] rowData = new Object[3];

		for (MenuItem o : restaurant.getMenuContent()) {
			rowData[0] = o.getItemId();
			rowData[1] = o.getItemName();
			rowData[2] = o.computePrice();
			model.addRow(rowData);
		}
		table.setModel(model);
	}

	// -----------------------------------

	public JButton getHome() {
		return home;
	}

	public void setHome(JButton home) {
		this.home = home;
	}

	public JRadioButton getRad1() {
		return rad1;
	}

	public void setRad1(JRadioButton rad1) {
		this.rad1 = rad1;
	}

	public JRadioButton getRad2() {
		return rad2;
	}

	public void setRad2(JRadioButton rad2) {
		this.rad2 = rad2;
	}

	public JTextField getItemName() {
		return itemName;
	}

	public void setItemName(JTextField itemName) {
		this.itemName = itemName;
	}

	public JTextField getItemPrice() {
		return itemPrice;
	}

	public String itemNameString() {
		return itemName.getText();
	}

	public String itemPriceString() {
		return itemPrice.getText();
	}

	public void setItemPrice(JTextField itemPrice) {
		this.itemPrice = itemPrice;
	}

	public JLabel getPriceLBL() {
		return priceLBL;
	}

	public void setPriceLBL(JLabel priceLBL) {
		this.priceLBL = priceLBL;
	}

	public JLabel getNameLBL() {
		return nameLBL;
	}

	public void setNameLBL(JLabel nameLBL) {
		this.nameLBL = nameLBL;
	}

	public JComboBox<MenuItem> getCreateComboBox() {
		return createComboBox;
	}

	public void setCreateComboBox(JComboBox<MenuItem> createComboBox) {
		this.createComboBox = createComboBox;
	}

	public JLabel getComponentsLBL() {

		return componentsLBL;
	}

	public void setComponentsLBL(JLabel componentsLBL) {
		this.componentsLBL = componentsLBL;
	}

	public TextArea getTextArea() {
		return textArea;
	}

	public void setTextArea(TextArea textArea) {
		this.textArea = textArea;
	}

	public JButton getAddBtn() {
		return addBtn;
	}

	public void setAddBtn(JButton addBtn) {
		this.addBtn = addBtn;
	}

	public JButton getCreateBtn() {
		return createBtn;
	}

	public void setCreateBtn(JButton createBtn) {
		this.createBtn = createBtn;
	}

	public JButton getCreateCompBtn() {
		return createCompBtn;
	}

	public void setCreateCompBtn(JButton createCompBtn) {
		this.createCompBtn = createCompBtn;
	}

	public IRestaurantProcessing getRestaurant() {
		return restaurant;
	}

	public void setRestaurant(IRestaurantProcessing restaurant) {
		this.restaurant = restaurant;
	}

	public JComboBox<MenuItem> getEditComboBox() {
		return editComboBox;
	}

	public void setEditComboBox(JComboBox<MenuItem> editComboBox) {
		this.editComboBox = editComboBox;
	}

	public JComboBox<MenuItem> getDeleteComboBox() {
		return deleteComboBox;
	}

	public void setDeleteComboBox(JComboBox<MenuItem> deleteComboBox) {
		this.deleteComboBox = deleteComboBox;
	}

	public String getEditItemName() {
		return eItemName.getText();
	}

	

	public String getEditItemPrice() {
		return eItemPrice.getText();
	}

	
	
	

}
