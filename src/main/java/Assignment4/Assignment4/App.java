package Assignment4.Assignment4;

import businessLayer.IRestaurantProcessing;
import businessLayer.Restaurant;
import dataLayer.RestaurantSerializator;
import presentation.DashboardGUI;
import presentation.GUIManager;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
       
    	RestaurantSerializator.setDestinationFile(args[0]);
        IRestaurantProcessing restaurant = RestaurantSerializator.deserializeRestaurant();
        GUIManager m=new GUIManager(restaurant,null);
    }
}
