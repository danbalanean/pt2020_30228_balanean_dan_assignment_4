package dataLayer;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

import businessLayer.MenuItem;
import businessLayer.Order;
import businessLayer.Restaurant;

/**				This class is used to generate output files as a result to the waiter's actions.
 * @author BalaneanDan
 * May 7, 2020
 */
public class FileWriter {
	private static int n; 	//number of current order 
	
	
	/**			This method is uset to generate the checkout bill for a order 
	 * @param o	Receives the specified order
	 * @param r	Receives the restaurant that should contain it
	 * @param file received file name
	 */
	public static void checkOutBill(Order o, Restaurant r) {
		n++;
		try {
			
			PrintWriter resultedFile = new PrintWriter("Bill" + n + ".txt");
			resultedFile.println("-----------------------------------\n");
			resultedFile.println("Order: \n\n"+"Bill number " + n);
			resultedFile.println("Table nr : " + o.getTable() + " ordered products: ");
			
			for (MenuItem item : r.getRequestedItems().get(o)) 
			{
				resultedFile.println(item.toString() + "  \n");
			}
			resultedFile.println("Total Price" + r.calculateOrderCost(o));
			resultedFile.println("-----------------------------------\n");
			resultedFile.close();
			
		} 
		catch(Exception e) 
		{
			System.out.println(e.getLocalizedMessage());
		}
	}
}
