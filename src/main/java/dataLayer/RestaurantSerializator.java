package dataLayer;

import java.io.FileInputStream;

import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.ObjectInputStream;

import java.io.FileOutputStream;


import businessLayer.Restaurant;

/**				This class implements methods to use the Serialization concept. 
 * @author BalaneanDan
 *May 7, 2020
 */
public class RestaurantSerializator implements Serializable  {
	
	private  static String destinationFile = "D:\\eclipse-workspace/Assignment4/restaurant.ser";
	
	
	public static String getDestinationFile() {
		return destinationFile;
	}


	public static void setDestinationFile(String destinationFile) {
		RestaurantSerializator.destinationFile = destinationFile;
	}


	/**			This method is used to save a received object to the restaurant.ser file 
	 * @param Receives the object that should be saved 
	 */
	public static void serializeRestaurant(Restaurant restaurant) {
		

		try {
			
			FileOutputStream resultedFile = new FileOutputStream(destinationFile);
			ObjectOutputStream oos = new ObjectOutputStream(resultedFile);
			oos.writeObject(restaurant);
			oos.close();
			resultedFile.close();
			
		} 
		catch (Exception e) 
		{

			System.out.println(e.getLocalizedMessage());
		}
	}

	
	/**
	 * 
	 * 	This methods is used to read a serialized object from the restaurant.ser file 
	 * 	@return It reurns the resulted object 
	 */
	public static Restaurant deserializeRestaurant() {

		Restaurant restaurant = new Restaurant();
		try {
			
			FileInputStream inputFile = new FileInputStream(destinationFile);
			ObjectInputStream ois = new ObjectInputStream(inputFile);
			restaurant = (Restaurant) ois.readObject();
			ois.close();
			inputFile.close();
			
		} 
		catch (Exception e) 
		{

			System.out.println(e.getLocalizedMessage());
		}
		return restaurant;
	}
	
}
